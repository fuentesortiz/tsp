﻿import pytest
from pytest import approx
from tsp import Place
from tsp.algorithms import AntColony, GreedyTSP
import numpy as np


def test_greedy(tsp_obj):

    g = GreedyTSP(initial=Place(3, 1, 1))

    solution = g.run(tsp_obj)

    assert int(solution['cost']) == 20


def test_aoc(tsp_obj):
    colony = AntColony()
    solution = colony.run(tsp_obj)
    print(solution['cost'])
    true_statement = 1750 <= int(solution['cost']) < = 2050
    assert true_statement

#-----------------------------------------------------------

def test_ant([tsp_obj]):
    colony = AntColony()
    place = Place(3,1,1)
    ant = Ant(colony, place)
    assert ant.current == place

def test_smell(tsp_obj):
    colony = AntColony()
    colony.ph = 1.0/len(tsp_obj.places)
    colony.phs = np.full([len(tsp_obj.places), len(tsp_obj.places)], fill_value=colony.ph)